import React, { useState, useEffect, createRef } from 'react';
import echarts from 'echarts';

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

import {csvParse} from 'd3-dsv';

import '../stylus/dashboard.styl';

import { fetchCSV, fetchJSON } from '../util/api';

let yearPriceData = [];
let detailScatterChart = null;
function PriceScoreDashboard() {
    const [chartIndex, setChartIndex] = useState(0);
    const [country, setCountry] = useState('US');
    const [wineNames, setWineNames] = useState([]);

    /**
     * @param {Array<any>} data
     * @param {number} index
     */
    function initAllScatterCharts(data, index) {
        const scatterChart = echarts.init(document.getElementById(`yqy__scatter-chart-${index}`));
        const ScatterChartOption = {
            title: {
                text: '点图标题',
                left: 'center',
                top: 0
            },
            visualMap: {
                dimension: 1,
                orient: 'vertical',
                right: 10,
                top: 'center',
                text: ['HIGH', 'LOW'],
                calculable: true,
                inRange: {
                    color: ['#f2c31a', '#24b7f2']
                }
            },
            tooltip: {
                trigger: 'item',
                axisPointer: {
                    type: 'cross'
                }
            },
            xAxis: [{
                type: 'value'
            }],
            yAxis: [{
                type: 'value',
                min: 78
            }],
            series: [{
                name: 'price-area',
                type: 'scatter',
                symbolSize: 5, // TODO: 点的大小
                data: data
            }]
        };
        scatterChart.setOption(ScatterChartOption);
    }
    // 切换第二步的酒
    function handleWineSelection(event) {
        const $element = event.target;
        if (!$element) return;
        const tagName = $element.tagName.toLowerCase();
        if (tagName === 'span' || tagName === 'li') {
            const wineName = $element.textContent;
            const index = yearPriceData.findIndex(({name: [name]}) => name === wineName);
            const {data} = yearPriceData[index];
            displayWineDetail(wineName, data);
        }
    }
    // 切换第二步的 地区(国家
    function handleCountrySelection(countryName) {
        setCountry(countryName);
        const countryData = yearPriceData.filter(({name: [, country]}) => {
            if (countryName === 'Others') {
                return !['US', 'Spain', 'Italy', 'France'].includes(country);
            } else {
                return country === countryName;
            }
        });
        if (countryData.length < 1) return;
        setWineNames(countryData.map(({name}) => name));
        displayWineDetail(countryData[0]['name'][0], countryData[0]['data']);
    }

    // 第二部 的 点图
    function displayWineDetail(title, data) {
        if (!detailScatterChart) return;
        const ScatterChartOption = {
            title: {
                text: title,
                left: 'center',
                top: 0
            },
            visualMap: {
                dimension: 1,
                orient: 'vertical',
                right: 10,
                top: 'center',
                text: ['HIGH', 'LOW'],
                calculable: true,
                inRange: {
                    color: ['#f2c31a', '#24b7f2']
                }
            },
            tooltip: {
                trigger: 'item',
                axisPointer: {
                    type: 'cross'
                }
            },
            xAxis: [{
                type: 'value',
                min: 1946,
                max: 2018
            }],
            yAxis: [{
                type: 'value',
            }],
            series: [{
                name: 'price-area',
                type: 'scatter',
                symbolSize: 25,
                data: data
            }]
        };
        detailScatterChart.setOption(ScatterChartOption);
    }
    useEffect(() => {
        Promise.all(
            [
                'US',
                'Spain',
                'Italy',
                'France',
                'Others',
            ].map(async name => {
                return {
                    name: name,
                    table: await fetchCSV(`data/${name}1.csv`)
                };
            })
        ).then(allData => {
            allData = allData.map(({name, table}) => ({
                name: name,
                table: csvParse(table),
            }));

            const pieChart = echarts.init(document.getElementById('yqy__pie-chart'));
            const pieChartOption = {
                title : {
                    text: '饼图标题',
                    subtext: '饼图副标题',
                    x:'center',
                    triggerEvent: true,
                },
                tooltip : {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                series : [
                    {
                        name: 'Country',
                        type: 'pie',
                        radius : '55%',
                        center: ['50%', '60%'],
                        data: allData.map(({name, table}) => ({name, value: table.length})),
                        itemStyle: {
                            emphasis: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }
                ]
            };
            pieChart.setOption(pieChartOption);
            pieChart.on('mousemove', ({dataIndex}) => {
                setChartIndex(dataIndex);
            });
            pieChart.on('click', (param) => {
                const { componentType } = param;
                switch (componentType) {
                    case 'title':
                        setChartIndex(-1);
                        break;
                    case 'series':
                        handleCountrySelection(param.name);
                        window.scrollTo(0, window.innerHeight / 2);
                        break;
                }
            });

            // 总表
            initAllScatterCharts(
                allData
                    .reduce((sum, {table}) => {
                        sum.push(...table);
                        return sum;
                    }, [])
                    .map(({price, rate}) => [price, rate]),
                -1
            );
            //  5 张表
            allData.forEach(({table}, index) => {
                setTimeout(() => {
                    initAllScatterCharts(table.map(({price, rate}) => [price, rate]), index)
                }, 100);
            });

            detailScatterChart = echarts.init(document.getElementById(`yqy__scatter-chart-detail`));
            // detail
            fetchJSON('data/year-price.json').then(json => {
                yearPriceData = json;
                setWineNames(json.map(({name}) => name).filter(([, country]) => country === 'US'));
                setTimeout(() => {
                    const {name, data} = json[0];
                    displayWineDetail(name[0], data);
                }, 2000);
            });
        });
    }, []);

    return (
        <section className="dashboard__layout">
            <div className="yqy__overview">
                <div id="yqy__pie-chart" className="yqy__pie-chart"></div>
                <div className="yqy__chart-container">
                    <div className={`yqy__scatter-chart ${chartIndex === -1 ? 'active' : ''}`}>
                        <div id="yqy__scatter-chart--1"></div>
                    </div>
                    <div className={`yqy__scatter-chart ${chartIndex === 0 ? 'active' : ''}`}>
                        <div id="yqy__scatter-chart-0"></div>
                    </div>
                    <div className={`yqy__scatter-chart ${chartIndex === 1 ? 'active' : ''}`}>
                        <div id="yqy__scatter-chart-1"></div>
                    </div>
                    <div className={`yqy__scatter-chart ${chartIndex === 2 ? 'active' : ''}`}>
                        <div id="yqy__scatter-chart-2"></div>
                    </div>
                    <div className={`yqy__scatter-chart ${chartIndex === 3 ? 'active' : ''}`}>
                        <div id="yqy__scatter-chart-3"></div>
                    </div>
                    <div className={`yqy__scatter-chart ${chartIndex === 4 ? 'active' : ''}`}>
                        <div id="yqy__scatter-chart-4"></div>
                    </div>
                </div>
            </div>
            <div className="yqy__detail-layout">
                <div className="yqy__controls">
                    <FormControl style={{width: '80%'}}>
                        <InputLabel htmlFor="country-selection">Country</InputLabel>
                        <Select
                            value={country}
                            onChange={event => handleCountrySelection(event.target.value)}
                            inputProps={{
                                name: 'country',
                                id: 'country-selection',
                            }}
                        >
                            <MenuItem value="US">US</MenuItem>
                            <MenuItem value="Spain">Spain</MenuItem>
                            <MenuItem value="Italy">Italy</MenuItem>
                            <MenuItem value="France">France</MenuItem>
                            <MenuItem value="Others">Others</MenuItem>
                        </Select>
                    </FormControl>
                    <ul className="yqy__controls__names" onClick={handleWineSelection}>
                        {wineNames.map(([name, country]) => (
                            <li key={`${name}-${country}`}>{name}</li>
                        ))}
                    </ul>
                </div>
                <div className="yqy__scatter-chart">
                    <div id="yqy__scatter-chart-detail"></div>
                </div>
            </div>
        </section>
    );
}

export default PriceScoreDashboard;
