import React, { useState } from 'react';
import ReactDOM from 'react-dom';

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';
import pink from '@material-ui/core/colors/pink';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import PriceScoreDashboard from './components/price_score';

// App Theme
const theme = createMuiTheme({
    palette: {
        primary: blue,
        secondary: pink,
    },
    typography: {
        useNextVariants: true,
    },
});

function App() {
    const [tabIndex, setTabIndex] = useState(0);
    return (
        <MuiThemeProvider theme={theme}>
            <Paper style={{flexGrow: 1, margin: 0, width: '100%'}}>
                <Tabs
                    value={tabIndex}
                    onChange={(_, value) => setTabIndex(value)}
                    indicatorColor="primary"
                    textColor="primary"
                    centered
                >
                    <Tab label="Dashboard" />
                </Tabs>
            </Paper>
            <div className="tab-container"
                style={{display: `${tabIndex === 0 ? 'block' : 'none'}`}}>
                <PriceScoreDashboard />
            </div>
        </MuiThemeProvider>
    );
}

ReactDOM.render(<App />, document.getElementById('root'));
