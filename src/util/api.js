/**
 * @fileoverview
 * API list
 */

/**
 * Fetch TEXT API
 * @param {string} filePath
 */
export function fetchCSV(filePath) {
    return new Promise((resolve, reject) => {
        fetch(filePath, {
            method: 'GET'
        }).then(
            response => response.text()
        ).then(data => {
            resolve(data);
        }).catch(err => reject(err));
    });
}

/**
 * Fetch JSON API
 * @param {string} filePath
 */
export function fetchJSON(filePath) {
    return new Promise((resolve, reject) => {
        fetch(filePath, {
            method: 'GET'
        }).then(
            response => response.json()
        ).then(data => {
            resolve(data);
        }).catch(err => reject(err));
    });
}

