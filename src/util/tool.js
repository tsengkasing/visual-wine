/**
 * @param {Date} dateTime
 * @returns {string}
 */
export function formatTime(dateTime) {
    const hour = dateTime.getHours().toString().padStart(2, '0');
    const minute = dateTime.getMinutes().toString().padStart(2, '0');
    const second = dateTime.getSeconds().toString().padStart(2, '0');
    return `${hour}:${minute}:${second}`;
}

/**
 * @param {Date} dateTime
 * @returns {string}
 */
export function formatDateTime(dateTime) {
    const year = dateTime.getFullYear();
    const month = dateTime.getMonth() + 1;
    const date = dateTime.getDate();
    const hour = dateTime.getHours().toString().padStart(2, '0');
    const minute = dateTime.getMinutes().toString().padStart(2, '0');
    const second = dateTime.getSeconds().toString().padStart(2, '0');
    return `${year}-${month}-${date} ${hour}:${minute}:${second}`;
}
